# Product management service

## Requirements

For building and running the application you need:
- [MySql 8.0](https://dev.mysql.com/doc/refman/8.0/en/)
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 4.0.0](https://maven.apache.org)

## Swagger documentation
- [Swagger Docs](http://localhost:8084/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/)


## Running the application locally
Inside project root directory type:
```shell
mvn spring-boot:run
```
