package com.engitronics.productmanagementservice.service;

import com.engitronics.productmanagementservice.model.Product;
import com.engitronics.productmanagementservice.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    List<Product> products = new  ArrayList<>();
    private Product product1;
    private Product product2;

    @InjectMocks
    ProductService productService;

    @BeforeEach
    public void setUp() {
        product1 = new Product (1L, "Knife Set", "A set of knives in all shapes and sizes.", 1L,new Date("19/09/2020 23:01:00") , new Date("19/09/2020 23:01:00"),  new Date("23/10/2020 23:01:00"));
        product2 = new Product (2L, "Plate Rack", "A storage solution for plates.", 3L,new Date("19/09/2020 23:01:00") , new Date("19/09/2020 23:01:00"),  new Date("23/10/2020 23:01:00"));

        products.add(product1);
        products.add(product2);
    }

    @AfterEach
    void tearDown() {
        products.clear();
    }

    @Test
    @DisplayName("Should retrieve product By id")
    public void ShouldRetrieveProductById() {
       when(productRepository.findById(1L)).thenReturn(Optional.of(product1));
       Optional<Product> response = productService.retrieveById(1L);
        assertAll(() -> {
            assertEquals(product1.getName(), response.get().getName());
            assertEquals(product1.getCategoryId(), response.get().getCategoryId());
                });
       verify(productRepository,times(1)).findById(1L);
    }

    @Test
    @DisplayName("Should create or update")
    void shouldCreateOrUpdate() {
        when(productRepository.save(product1)).thenReturn(product1);
        Product  newProduct = productService.createOrUpdate(product1);
        assertEquals(product1, newProduct);
    }

    @Test
    @DisplayName("Should delete by id")
    void shouldDeleteById() {
        productService.deleteById(1L);
        verify(productRepository, times(1)).deleteById(1L);
    }

    @Test
    void shouldRetrieveByCategoryId() {
        when(productRepository.findByCategoryId(1L)).thenReturn(Stream.of(product1,product2, new Product (1L, "Knife Set", "A set of knives in all shapes and sizes.", 1L,new Date("19/09/2020 23:01:00") , new Date("19/09/2020 23:01:00"),  new Date("23/10/2020 23:01:00"))).collect(Collectors.toList()));
        List<Product> productList = productService.retrieveByCategory(1L);
        assertEquals(2,productList.size()-1);
    }
}