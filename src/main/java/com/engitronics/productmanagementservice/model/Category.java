package com.engitronics.productmanagementservice.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "name unique identifier", example = "Kitchen", required = true)
    //@NotNull(message = "Name can not be null!")
   // @NotEmpty(message = "Name can not be empty!")
    private String name;

}
