package com.engitronics.productmanagementservice.model;

import com.engitronics.productmanagementservice.utils.Constants;
import com.engitronics.productmanagementservice.validation.DateV;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@DateV
@Entity
@Table(name = "product")
@Data
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "name identifier", example = "Knife Set", required = true)
    @NotNull
    @NotEmpty
    private String name;

    @Schema(description = "Product description", example = "A set of knives in all shapes and sizes.", required = true)
    @NotNull(message = "Description can not be null!")
    @NotEmpty(message = "Description can not be empty!")
    private String description;

    @Column(name = "category_id" )
    @Schema(description = "Product category from category list", example = "1", required = true)
    private Long categoryId;

    @Column(name = "creation_date")
    @Schema(description = "Creation Date", example = "2/07/2020 10:34:22", required = true)
    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @CreatedDate
    private Date creationDate;

    @Column(name = "updated_date")
    @Schema(description = "Updated Date", example = "2/07/2020 10:34:22", required = true)
    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @LastModifiedDate
    private Date updatedDate;

    @Column(name = "last_purchased_date")
    @Schema(description = "purchased Date", example = "2/07/2020 10:34:22", required = true)
    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    private Date lastPurchasedDate;

    public Product() {
    }

    public Product(Long id, String name, String description, Long categoryId, Date creationDate, Date updatedDate, Date lastPurchasedDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.creationDate = creationDate;
        this.updatedDate = updatedDate;
        this.lastPurchasedDate = lastPurchasedDate;
    }

    @PrePersist
    protected void onCreate(){
        this.creationDate = new Date();
    }
    @PreUpdate
    protected void onUpdate(){
        this.updatedDate=  new Date();
    }

}
