package com.engitronics.productmanagementservice.exceptions;

public class NotFoundException extends RuntimeException{
    private static final long SerialVersionUID = 1L;

    public NotFoundException(String message){
        super(message);
    }
}
