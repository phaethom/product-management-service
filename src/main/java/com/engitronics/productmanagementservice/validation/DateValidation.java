package com.engitronics.productmanagementservice.validation;

import com.engitronics.productmanagementservice.model.Product;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidation  implements ConstraintValidator<DateV, Product> {
    @Override
    public void initialize(DateV constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Product product, ConstraintValidatorContext constraintValidatorContext) {
        return product.getCreationDate().before(product.getLastPurchasedDate());
    }
}
