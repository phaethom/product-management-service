package com.engitronics.productmanagementservice.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateValidation.class)
public @interface DateV {
    String message() default "Last Purchased date cannot be less than creation date";
    Class<?>[] groups() default {};
    public abstract Class<? extends Payload>[] payload() default {};
}
