package com.engitronics.productmanagementservice.service;

import com.engitronics.productmanagementservice.model.Category;
import com.engitronics.productmanagementservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Page<Category> retrieveAll(Pageable pageable){
        return categoryRepository.findAll(pageable);
    }

    public Optional<Category> retrieveById(long id){
       return categoryRepository.findById(id);
    }

    public Category CreateOrUpdate(Category category){
        return categoryRepository.save(category);
    }

    public void deleteById(long id){
        categoryRepository.deleteById(id);
    }
}
