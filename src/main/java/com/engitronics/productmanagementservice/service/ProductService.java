package com.engitronics.productmanagementservice.service;

import com.engitronics.productmanagementservice.model.Product;
import com.engitronics.productmanagementservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Page<Product> retrieveAll(Pageable pageable){
        return productRepository.findAll(pageable);
    }

    public Optional<Product> retrieveById(long id){
        return productRepository.findById(id);
    }

    public Product createOrUpdate(Product product){
        return productRepository.save(product);
    }

    public  void deleteById(long id){
        productRepository.deleteById(id);
    }

    public List<Product> retrieveByCategory(long categoryId){
        return productRepository.findByCategoryId(categoryId);
    }
}
