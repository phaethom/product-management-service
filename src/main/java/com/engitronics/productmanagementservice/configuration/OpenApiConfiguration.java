package com.engitronics.productmanagementservice.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfiguration {
    @Bean
    public OpenAPI customApi(@Value("${springdoc.version}") String appVersion){
        return new OpenAPI()
                .components(new Components())
                .info(appInfo());
    }

    private Info appInfo(){
        return new Info()
                .title("Product Management Service")
                .description("Product Management Service")
                .version("1.0.0")
                .contact(contact())
                .license(license());
    }

    private Contact contact(){
        return new Contact().name("Leisure Support")
                .email("info@leisurepassgroup.com")
                .url("https://www.leisurepassgroup.com");
    }

    private License license(){
        return new License().name("Leisure pass group License Version 0.0.1")
                .url("https://www.leisurepassgroup.com");
    }
}
