package com.engitronics.productmanagementservice.utils;

public class Constants {

    public static final String LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
}
