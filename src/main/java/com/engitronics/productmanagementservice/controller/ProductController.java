package com.engitronics.productmanagementservice.controller;

import com.engitronics.productmanagementservice.exceptions.NotFoundException;
import com.engitronics.productmanagementservice.model.Product;
import com.engitronics.productmanagementservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3005")
@RestController
@RequestMapping(value ="/api/v1" )
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping(value = "/products")
    public ResponseEntity<Product> createOrUpdate(@Valid @RequestBody Product product){
        Product product1= productService.createOrUpdate(product);
        return new ResponseEntity<Product>(product1, HttpStatus.CREATED);
    }

    @GetMapping(value = "/products/{id}")
    public ResponseEntity<Product> retrieveById(@PathVariable long id){
        Product product = productService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Product with id %s not found!", id)));
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping(value = "/products")
    public ResponseEntity retrieveAll(Pageable pageable){
        return new ResponseEntity(productService.retrieveAll(pageable), new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/products/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id){
        productService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Product with id %s not found!",id)));
        productService.deleteById(id);
        return new ResponseEntity<>("Deleted",HttpStatus.OK);
    }

    @PutMapping(value = "/products/{id}")
    public ResponseEntity<Product> updateById(@PathVariable Long id, @RequestBody @Valid Product product ){
        productService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Product with id %s not found!",id)));
        product.setId(id);
        return new ResponseEntity<>(productService.createOrUpdate(product),HttpStatus.OK);
    }

    @GetMapping(value = "/products/category/{categoryId}")
    public ResponseEntity<List<Product>> retrieveByCategory(@PathVariable long categoryId){
        return new ResponseEntity<List<Product>>(productService.retrieveByCategory(categoryId), HttpStatus.OK);
    }

    @PatchMapping(value = "/products/{id}")
    public ResponseEntity<Product> updateField(@PathVariable long id, @RequestBody Map<Object, Long> fields){
        Product product = productService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Product with id %s not found!",id)));
        fields.forEach((K, v)-> {
            Field field = ReflectionUtils.findField(Product.class, String.valueOf(v));
            field.setAccessible(true);
            ReflectionUtils.setField(field, product, v);
        });
       Product product1=productService.createOrUpdate(product);
        product.setId(id);
        return new ResponseEntity<>(product1,HttpStatus.OK);

    }
}
