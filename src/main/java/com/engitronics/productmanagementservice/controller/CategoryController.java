package com.engitronics.productmanagementservice.controller;

import com.engitronics.productmanagementservice.exceptions.NotFoundException;
import com.engitronics.productmanagementservice.model.Category;
import com.engitronics.productmanagementservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:3005")
@RestController
@RequestMapping(value ="/api/v1" )
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @PostMapping(value = "/categories")
    public ResponseEntity<Category> createOrUpdate(@Valid @RequestBody Category category){
        Category category1 = categoryService.CreateOrUpdate(category);
        return new ResponseEntity<Category>(category1, HttpStatus.CREATED);
    }

    @GetMapping(value = "/categories/{id}")
    public ResponseEntity<Category> retrieveById(@PathVariable long id){
        Category category = categoryService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Device with id %s not found!", id)));
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @GetMapping(value = "/categories")
    public ResponseEntity<ArrayList<Category>> retrieveAll(Pageable pageable){
        return new ResponseEntity(categoryService.retrieveAll(pageable), new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/categories/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id){
        categoryService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Category with id %s not found!",id)));
        categoryService.deleteById(id);
        return new ResponseEntity<>("Deleted",HttpStatus.OK);
    }

    @PutMapping(value = "/categories/{id}")
    public ResponseEntity<Category> updateById(@PathVariable long id, @RequestBody @Valid Category category ){
        categoryService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Category with id %s not found!",id)));
        category.setId(id);
        return new ResponseEntity<>(categoryService.CreateOrUpdate(category),HttpStatus.OK);
    }
}
