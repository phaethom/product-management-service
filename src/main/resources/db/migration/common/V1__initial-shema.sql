SET time_zone = "+00:00";


create table if not exists category(
    id INT(50) NOT NULL auto_increment,
    name VARCHAR(100)NOT NULL,
    PRIMARY KEY (id)
)engine=innodb;

create table if not exists product(
id INT(50) NOT NULL PRIMARY KEY auto_increment,
 name VARCHAR(30) NOT NULL,
 description VARCHAR (100) NOT NULL,
 category_id INT NOT NULL,
 creation_date DATETIME,
 updated_date DATETIME,
 last_purchased_date DATETIME,
 FOREIGN KEY (category_id) REFERENCES category (id)
)engine=innodb;
