FROM openjdk:11
RUN mkdir /app
ADD target/product-management.jar app/product-management.jar
ENTRYPOINT ["java","-jar","/app/product-management.jar"]